from flask import Flask, request, render_template

app = Flask(__name__)


@app.route('/')
def hello_world():
    return "Hello, World"


@app.route('/hi/')
def hi():
    name = request.args.get('name')
    return f'<h1>Hi {name}</h1>'

@app.route('/template/')
def template():
    values = [1, 2, 3, 4]
    

    return render_template('template.html', name='hello', values=values)


if __name__ == '__main__':
    app.run(debug=True)