from catalog import main
from catalog.models import Book

from flask import render_template

@main.route('/')
def display_books():
    books = Book.query.all()
    return render_template('home.html', books=books)