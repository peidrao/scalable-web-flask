from datetime import datetime
from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)

app.config.update(
    SQLALCHEMY_DATABASE_URI= 'sqlite:///db.sqlite3',
    SECRET_KEY='app_flask_test',
    SQLALCHEMY_TRACK_MODIFICATIONS=False
    )

db = SQLAlchemy(app)

class Publication(db.Model):
    __tablename__ = 'publications'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)

    def __init__(self, id, name):
        self.id = id
        self.name = name

    def __repr__(self) -> str:
        return f'This id is {self.id}, Name is {self.name}'


class Book(db.Model):
    __tablename__ = 'books'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200), nullable=False, index=True)
    author = db.Column(db.String(350))
    rating = db.Column(db.Float)
    format = db.Column(db.String(50))
    num_pages = db.Column(db.Integer)
    image = db.Column(db.String(100), unique=True)
    pub_date = db.Column(db.DateTime, default=datetime.utcnow())

    pub_id = db.Column(db.Integer, db.ForeignKey('publications.id'))

    def __init__(self, title, author, rating, format, num_pages, image, pub_id):
        self.title = title
        self.author = author
        self.rating = rating
        self.format = format
        self.num_pages = num_pages
        self.image = image
        self.pub_id = pub_id

if __name__ == '__main__':
    db.create_all()
    app.run(debug=True)